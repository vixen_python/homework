import functools


def in_html_tag(tag: str):
    """decorator for adding html tags to a function that returns a string
    only suitable to create paired tags e.g. <p>, <div>, <ol>, etc."""
    def inner(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            value = func(*args, **kwargs)
            tag_value = f'<{tag}>{value}</{tag}>'
            return func(tag_value)
        return wrapper
    return inner


@in_html_tag(tag='h1')
def return_text(text: str) -> str:
    return text


if __name__ == "__main__":
    print(return_text('This is a h1 tag'))
