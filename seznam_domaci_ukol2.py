def is_prime(n: int) -> bool:
    """Primality test using 6k+-1 optimization
    simplified for max_six_digit_prime function"""
    if n % 3 == 0:
        return False
    i = 5
    while i ** 2 <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True


def max_six_digit_prime() -> int:
    """Function returns the largest six digit prime number"""
    for number in range(999999, 100000, -2):
        if is_prime(number):
            return number


if __name__ == '__main__':
    print(max_six_digit_prime())
